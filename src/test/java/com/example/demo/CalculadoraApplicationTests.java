package com.example.demo;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CalculadoraApplicationTests {

	@Test
	public void contextLoads() {
	}
	
	@Test
	public void divideTest1() {
		double divisao = CalculadoraApplication.divide(18,3);
		Assert.assertEquals(6.0, divisao, 0.1);
	}

	@Test
	public void divideTest2() {
		double divisao = CalculadoraApplication.divide(22.2,2.0);
		Assert.assertEquals(11.0, divisao, 0.1);
	}	
	
	@Test
	public void divideTest3() {
		double divisao = CalculadoraApplication.divide(45.5, 1.2);
		Assert.assertEquals(37.9, divisao, 0.1);
	}
	
	@Test
	public void raizCubicaTest1() {
		double raiz = CalculadoraApplication.raizCubica(1);
		Assert.assertEquals(1.0, raiz, 0.1);
	}
	
	@Test
	public void raizCubicaTest2() {
		double raiz = CalculadoraApplication.raizCubica(125);
		Assert.assertEquals(5.0, raiz, 0.1);
	}
	
	@Test
	public void raizCubicaTest3() {
		double raiz = CalculadoraApplication.raizCubica(120);
		Assert.assertEquals(4.93, raiz, 0.1);
	}
	
	@Test
	public void raizCubicaTest4() {
		double raiz = CalculadoraApplication.raizCubica(384);
		Assert.assertEquals(7.2, raiz, 0.1);
	}

	@Test
	public void potenciaTest1() {
		double divisao = CalculadoraApplication.potencia(2, 3);
		Assert.assertEquals(8, divisao, 0.1);
	}
	
	@Test
	public void potenciaTest2() {
		double divisao = CalculadoraApplication.potencia(2, 5);
		Assert.assertEquals(32, divisao, 0.1);
	}
	
	@Test
	public void potenciaTest3() {
		double divisao = CalculadoraApplication.potencia(3, 3);
		Assert.assertEquals(27, divisao, 0.1);
	}

	@Test
	public void multiplicaTest1() {
		double resultado = CalculadoraApplication.multiplica(2, 3);
		Assert.assertEquals(6, resultado, 0.1);
	}
	
	@Test
	public void multiplicaTest2() {
		double resultado = CalculadoraApplication.multiplica(10, 10);
		Assert.assertEquals(100, resultado, 0.1);
	}
	
	@Test
	public void multiplicaTest3() {
		double resultado = CalculadoraApplication.multiplica(2.5, 2.5);
		Assert.assertEquals(6.25, resultado, 0.1);
	}
	
}
